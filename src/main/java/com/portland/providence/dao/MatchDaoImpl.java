package com.portland.providence.dao;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.portland.common.constant.CacheTTL;
import com.portland.common.constant.Catalog;
import com.portland.common.dao.MatchDao;
import com.portland.common.pojo.ListResult;
import com.portland.providence.util.DaoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.portland.common.domain.Match;
import com.portland.common.pojo.Result;
import com.portland.providence.auth.Permission;


/**
 * @author uriel
 * Match DAO
 */
@Repository
public class MatchDaoImpl extends EntityDao<Match> implements MatchDao {
    
    @Autowired
    private Permission permission;
    
    private static final Logger logger = LoggerFactory.getLogger(MatchDaoImpl.class);
    
    @Override
    public Result<Match> insert(Match match) {
        String newUuid = UUID.randomUUID().toString();
        try {
            provJdbcTemplate.update(
                    "INSERT INTO " + getTableName()
                    + "(uuid, permalink, date_scheduled, date_start,"
                    + "date_end, home_score, away_score, home_penalties,"
                    + "away_penalties, home_team_id, away_team_id, matchday_id, "
                    + "stadium_id, status, date_created, date_modified,"
                    + "user_id )" + " VALUES " + "(?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?)",
                    newUuid, match.getPermalink(), match.getDateScheduled(), match.getDateStart(),
                    match.getDateEnd(), match.getHomeScore(), match.getAwayScore(), match.getHomePenalties(),
                    match.getAwayPenalties(), match.getHomeTeamId(), match.getAwayTeamId(), match.getMatchdayId(),
                    match.getStadiumId(), match.getStatus(), Timestamp.from(Instant.now()), Timestamp.from(Instant.now()),
                    permission.getUser().get().getId());
            log.message("Inserting " + getRestName()).item(match).debug(logger);
            return getByUuid(newUuid);
        } catch (Exception e) {
            e.printStackTrace();
            log.message("Could not insert " + getRestName()).item(match).exception(e).error(logger);
            return Result.failedResult(getType(), "Could not insert " + getRestName() + e.getMessage());
        }
    }
    
    @Override
    public Result<Match> update(Match match) {
        try {
            provJdbcTemplate.update("UPDATE " + getTableName() + " SET "
                    + "date_scheduled = ?, date_start = ?, date_end = ?, home_score = ?, "
                    + "away_score = ?," + "home_penalties = ?, away_penalties = ?, home_team_id = ?, "
                    + "away_team_id = ?, matchday_id = ?, stadium_id = ?, status = ?,"
                    + "date_modified = ?"
                    + "WHERE id = ?",
                    match.getDateScheduled(), match.getDateStart(), match.getDateEnd(), match.getHomeScore(),
                    match.getAwayScore(), match.getHomePenalties(), match.getAwayPenalties(), match.getHomeTeamId(),
                    match.getAwayTeamId(), match.getMatchdayId(), match.getStadiumId(), match.getStatus(),
                    Timestamp.from(Instant.now()),
                    match.getId());
            log.message("Updating " + getRestName()).item(match).debug(logger);
            return getByUuid(match.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            log.message("Could not update " + getRestName()).item(match).exception(e).error(logger);
            return Result.failedResult(getType(), "Could not update " + getRestName() + e.getMessage());
        }
    }

    @Override
    public ListResult<Match> getMatchesByDate(String date, Integer page, Integer size, String order) {
        Optional<String> validatedOrder = DaoUtil.validateOrderParam(getType(), order);
        if (!validatedOrder.isPresent()) {
            return ListResult.failedResult(getType(), "Invalid order param");
        }
        String sql = "SELECT * FROM " + getTableName() + " WHERE date_format(date_scheduled, '%Y-%m-%d') = ? ORDER BY " + validatedOrder.get() + " LIMIT ?, ?";
        try {
            List<Match> matches = provJdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<Match>(getType()), date, (page * size), size);
            log.message("Getting  list " + getRestName() + " {}", matches.size()).debug(logger);
            return ListResult.successResult(matches, getMatchesCountByDate(date));
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " items found.").debug(logger);
        }
        return ListResult.failedResult(getType(), "Could not get " + getRestName() + " list");
    }

    @Override
    public Integer getMatchesCountByDate(String date) {
        String sql = "SELECT COUNT(*) FROM " + getTableName() + " WHERE date_format(date_scheduled, '%Y-%m-%d') = ?";
        return provJdbcTemplate.queryForObject(sql, Integer.class, date);
    }

    @Override
    public ListResult<Match> getMatchesByTeamBetweenPastDateAndFutureDate(Long teamId, String pastDate, String futureDate,
                                                                             Integer page, Integer size, String order) {
        Optional<String> validatedOrder = DaoUtil.validateOrderParam(getType(), order);
        if (!validatedOrder.isPresent()) {
            return ListResult.failedResult(getType(), "Invalid order param");
        }
        String sql = "SELECT * FROM " + getTableName() + " WHERE (away_team_id = ? OR home_team_id = ?) AND "
                + "(date_scheduled BETWEEN ? AND ?) AND (status != " + Catalog.INACTIVE + ") ORDER BY " + validatedOrder.get() + " LIMIT ?, ?";
        try {
            List<Match> matches = provJdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<Match>(getType()), teamId, teamId, pastDate, futureDate, (page * size), size);
            log.message("Getting  list " + getRestName() + " {}", matches.size()).debug(logger);
            return ListResult.successResult(matches, getMatchesCountByTeamBetweenPastDateAndFutureDate(teamId,
                    pastDate, futureDate).getValue().get());
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " items found.").debug(logger);
        }
        return ListResult.failedResult(getType(), "Could not get " + getRestName() + " list");
    }

    @Override
    public ListResult<Match> getMatchesByTeamBetweenPastDateAndFutureDateFromCache(Long teamId, String pastDate, String futureDate,
                                                                                      Integer page, Integer size, String order) {
        return cacheUtil.getFromCache("match-team-list", getRestName() + "-teamId-" + teamId
                        + "-size-" + size + "-pastDate-" + pastDate + "-futureDate-" + futureDate,
                CacheTTL.LIST.getTtl(), getMatchesCountByTeamBetweenPastDateAndFutureDateFromCache(teamId,
                        pastDate, futureDate).getValue().get(), () -> getMatchesByTeamBetweenPastDateAndFutureDate(teamId,
                        pastDate, futureDate, page, size, order));
    }

    @Override
    public Result<Integer> getMatchesCountByTeamBetweenPastDateAndFutureDate(Long teamId, String pastDate, String futureDate) {
        String sql = "SELECT COUNT(*) FROM " + getTableName() + " WHERE (away_team_id = ? OR home_team_id = ?) "
                + "AND (date_scheduled BETWEEN ? AND ?) AND (status != " + Catalog.INACTIVE + ")";
        return Result.successResult(provJdbcTemplate.queryForObject(sql, Integer.class, teamId, teamId, pastDate, futureDate));
    }

    @Override
    public Result<Integer> getMatchesCountByTeamBetweenPastDateAndFutureDateFromCache(Long teamId, String pastDate, String futureDate) {
        return cacheUtil.getFromCache("counter-match-team-list", getRestName() + "-teamId-" + teamId
                        + "-pastDate-" + pastDate + "-futureDate-" + futureDate,
                CacheTTL.LIST.getTtl(), () -> getMatchesCountByTeamBetweenPastDateAndFutureDate(teamId,
                        pastDate, futureDate));
    }

    @Override
    public Result<Match> getLiveMatchByTeamId(Long teamId) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE (away_team_id = ? OR home_team_id = ?) AND (status = " + Catalog.MATCH_STATUS_LIVE + ") LIMIT 1";
        try {
            Match entity = (Match)provJdbcTemplate.queryForObject(sql,
                    new BeanPropertyRowMapper<Match>(getType()), teamId, teamId);
            log.message("Getting " + getRestName() + " with away_team_id or home_team_id: {}", json.toJson(entity).get()).debug(logger);
            return Result.successResult(entity);
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " with id: {}", teamId).debug(logger);
        }
        return Result.failedResult(getType(), "Could not get " + getRestName() + " by id");
    }

    @Override
    public Result<Match> getLiveMatchByTeamIdFromCache(Long teamId) {
        return cacheUtil.getFromCache("team-live-match-", getRestName() + "-teamId-" + teamId,
                CacheTTL.LIVE_MATCH.getTtl(), () -> getLiveMatchByTeamId(teamId));
    }

    @Override
    public ListResult<Match> getMatchesBetweenPastDateAndFutureDate(String pastDate, String futureDate,
                                                                    Integer page, Integer size, String order) {
        Optional<String> validatedOrder = DaoUtil.validateOrderParam(getType(), order);
        if (!validatedOrder.isPresent()) {
            return ListResult.failedResult(getType(), "Invalid order param");
        }
        String sql = "SELECT * FROM " + getTableName() + " WHERE "
                + "(date_scheduled BETWEEN ? AND ?) AND (status != " + Catalog.INACTIVE + ") ORDER BY " + validatedOrder.get() + " LIMIT ?, ?";
        try {
            List<Match> matches = provJdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<Match>(getType()), pastDate, futureDate, (page * size), size);
            log.message("Getting  list " + getRestName() + " {}", matches.size()).debug(logger);
            return ListResult.successResult(matches, getMatchesCountBetweenPastDateAndFutureDate(
                    pastDate, futureDate).getValue().get());
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " items found.").debug(logger);
        }
        return ListResult.failedResult(getType(), "Could not get " + getRestName() + " list");
    }

    @Override
    public ListResult<Match> getMatchesBetweenPastDateAndFutureDateFromCache(String pastDate, String futureDate,
                                                                             Integer page, Integer size, String order) {
        return cacheUtil.getFromCache("match-list", getRestName() + "-size-" + size
                        + "-pastDate-" + pastDate + "-futureDate-" + futureDate,
                CacheTTL.LIST.getTtl(), getMatchesCountBetweenPastDateAndFutureDateFromCache(
                        pastDate, futureDate).getValue().get(), () -> getMatchesBetweenPastDateAndFutureDate(
                        pastDate, futureDate, page, size, order));
    }

    @Override
    public Result<Integer> getMatchesCountBetweenPastDateAndFutureDate(String pastDate, String futureDate) {
        String sql = "SELECT COUNT(*) FROM " + getTableName() + " WHERE (date_scheduled BETWEEN ? AND ?) AND (status != " + Catalog.INACTIVE + ")";
        return Result.successResult(provJdbcTemplate.queryForObject(sql, Integer.class, pastDate, futureDate));
    }

    @Override
    public Result<Integer> getMatchesCountBetweenPastDateAndFutureDateFromCache(String pastDate, String futureDate) {
        return cacheUtil.getFromCache("counter-match-list", getRestName()
                        + "-pastDate-" + pastDate + "-futureDate-" + futureDate,
                CacheTTL.LIST.getTtl(), () -> getMatchesCountBetweenPastDateAndFutureDate(pastDate, futureDate));
    }

    @Override
    public ListResult<Match> getMatchesByMatchdayWithStadium(Long matchdayId, Integer page, Integer size, String order) {
        Optional<String> validatedOrder = DaoUtil.validateOrderParam(getType(), order);
        if (!validatedOrder.isPresent()) {
            return ListResult.failedResult(getType(), "Invalid order param");
        }
        String sql = "SELECT * FROM " + getTableName() + " WHERE " +
                "matchday_id = ? AND (status != " + Catalog.INACTIVE + ") AND stadium_id IS NOT NULL ORDER BY "
                + validatedOrder.get() + " LIMIT ?, ?";
        try {
            List<Match> matches = provJdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<Match>(getType()), matchdayId, (page * size), size);
            log.message("Getting list " + getRestName() + " {}", matches.size()).debug(logger);
            return ListResult.successResult(matches, getMatchesCountByMatchdayWithStadium(matchdayId).getValue().get());
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " items found.").debug(logger);
        }
        return ListResult.failedResult(getType(), "Could not get " + getRestName() + " list");

    }

    @Override
    public Result<Integer> getMatchesCountByMatchdayWithStadium(Long matchdayId) {
        String sql = "SELECT COUNT(*) FROM " + getTableName() + " WHERE matchday_id = ? AND stadium_id IS NOT NULL " +
                "AND (status != " + Catalog.INACTIVE + ")";
        return Result.successResult(provJdbcTemplate.queryForObject(sql, Integer.class, matchdayId));
    }

    @Override
    public String getRestName() {
        return "match";
    }

    @Override
    public Class<Match> getType() {
        return Match.class;
    }

    @Override
    public String getTableName() {
        return "portland.match";
    }
    
}
