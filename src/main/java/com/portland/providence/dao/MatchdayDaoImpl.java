package com.portland.providence.dao;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import com.portland.common.constant.Catalog;
import com.portland.common.dao.MatchdayDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.portland.common.domain.Matchday;
import com.portland.common.pojo.Result;
import com.portland.providence.auth.Permission;


/**
 * @author uriel
 * Matchday DAO
 */
@Repository
public class MatchdayDaoImpl extends EntityDao<Matchday> implements MatchdayDao {
    
    @Autowired
    private Permission permission;
    
    private static final Logger logger = LoggerFactory.getLogger(MatchdayDaoImpl.class);
    
    @Override
    public Result<Matchday> insert(Matchday matchday) {
        
        String newUuid = UUID.randomUUID().toString();
        try {
            provJdbcTemplate.update(
                    "INSERT INTO " + getTableName()
                    + "(uuid,number,name,date_start,"
                    + "date_end, status, date_created, date_modified,"
                    + "season_id, user_id)" + " VALUES " + "(?,?,?,?, ?,?,?,?, ?,?)",
                    newUuid, matchday.getNumber(), matchday.getName(), matchday.getDateStart(),
                    matchday.getDateEnd(), matchday.getStatus(), Timestamp.from(Instant.now()), Timestamp.from(Instant.now()),
                    matchday.getSeasonId(), permission.getUser().get().getId());
            log.message("Inserting " + getRestName()).item(matchday).debug(logger);
            return getByUuid(newUuid);
        } catch (Exception e) {
            e.printStackTrace();
            log.message("Could not insert " + getRestName()).item(matchday).exception(e).error(logger);
            return Result.failedResult(getType(), "Could not insert " + getRestName() + e.getMessage());
        }

    }
    
    @Override
    public Result<Matchday> update(Matchday matchday) {
        try {
            provJdbcTemplate.update("UPDATE " + getTableName() + " SET " 
                    + "number = ?, name = ?, date_start = ?, date_end = ?,"
                    + " status = ?, date_modified = ?," + "season_id = ?, user_id = ? "
                    + "WHERE id = ?",
                    matchday.getNumber(), matchday.getName(), matchday.getDateStart(), matchday.getDateEnd(),
                    matchday.getStatus(), Timestamp.from(Instant.now()), matchday.getSeasonId(),  permission.getUser().get().getId(),
                    matchday.getId());
            log.message("Updating " + getRestName()).item(matchday).debug(logger);
            return getByUuid(matchday.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            log.message("Could not update " + getRestName()).item(matchday).exception(e).error(logger);
            return Result.failedResult(getType(), "Could not update " + getRestName() + e.getMessage());
        }
    }

    @Override
    public Result<Matchday> getByNumber(Integer number) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE number = ? AND status != " + Catalog.INACTIVE;
        try {
            Matchday entity = (Matchday)provJdbcTemplate.queryForObject(sql,
                    new BeanPropertyRowMapper<Matchday>(getType()), number);
            log.message("Getting " + getRestName() + " with number " + number + ": {}", json.toJson(entity).get()).debug(logger);
            return Result.successResult(entity);
        } catch (EmptyResultDataAccessException e) {
            log.message("NO " + getRestName() + " with number: {}", number).debug(logger);
        }
        return Result.failedResult(getType(), "Could not get entity by number");
    }
    
    @Override
    public String getRestName() {
        return "matchday";
    }

    @Override
    public Class<Matchday> getType() {
        return Matchday.class;
    }

    @Override
    public String getTableName() {
        return "matchday";
    }
}
