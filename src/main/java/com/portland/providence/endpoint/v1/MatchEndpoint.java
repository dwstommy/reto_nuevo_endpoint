package com.portland.providence.endpoint.v1;


import com.portland.common.domain.Match;
import com.portland.common.pojo.ListResult;
import com.portland.common.pojo.Result;
import com.portland.common.util.EndpointUtil;
import com.portland.providence.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * 
 * @author mklfarha
 *
 */
@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class MatchEndpoint {
    
    @Autowired
    private MatchService matchService;
    
    @GET
    @Path("/match/list")
    public Response listEntity(@DefaultValue("0") @QueryParam("page") Integer page, @DefaultValue("10") @QueryParam("size") Integer size,
            @DefaultValue("date_created desc") @QueryParam("order") String order, @Context HttpServletRequest request) {

            ListResult<Match> result = matchService.listEntity(page, size, order);
            if (result.isValid()) {
                result.getValues().get().stream().forEach(match -> matchService.setUpMatch(match));
            }
            return EndpointUtil.getListResponse(result, page, size, Optional.empty());
        
    }


    @GET
    @Path("/match/list/{date}")
    public Response getMatchesByDate(@DefaultValue("0") @QueryParam("page") Integer page,
                               @DefaultValue("10") @QueryParam("size") Integer size,
                               @DefaultValue("date_scheduled ASC") @QueryParam("order") String order,
                               @PathParam("date") String date,
                               @Context HttpServletRequest request) {

        ListResult<Match> result = matchService.getMatchesByDate(date, page, size, order);
        return EndpointUtil.getListResponse(result, page, size, Optional.empty());
    }

    @GET
    @Path("/team/match/list/{uuid}")
    public Response getMatchesByTeamBetweenPastDateAndFutureDate(@DefaultValue("0") @QueryParam("page") Integer page,
                                     @DefaultValue("10") @QueryParam("size") Integer size,
                                     @DefaultValue("date_scheduled ASC") @QueryParam("order") String order,
                                     @PathParam("uuid") String teamUuid,
                                     @Context HttpServletRequest request) {

        ListResult<Match> result = matchService.getMatchesByTeamBetweenPastDateAndFutureDate(teamUuid, page, size, order);
        return EndpointUtil.getListResponse(result, page, size, Optional.empty());


    }

    @GET
    @Path("/team/live/match/{uuid}")
    public Response getLiveMatchByTeam(@PathParam("uuid") String teamUuid) {

        Result<Match> result = matchService.getLiveMatchByTeamUuid(teamUuid);
        return EndpointUtil.getResponse(result);
    }

    @GET
    @Path("/match/home/list")
    public Response getMatchesBetweenPastDateAndFutureDate(@DefaultValue("0") @QueryParam("page") Integer page,
                                                                 @DefaultValue("10") @QueryParam("size") Integer size,
                                                                 @DefaultValue("date_scheduled ASC") @QueryParam("order") String order,
                                                                 @Context HttpServletRequest request) {

        ListResult<Match> result = matchService.getMatchesBetweenPastDateAndFutureDate(page, size, order);
        return EndpointUtil.getListResponse(result, page, size, Optional.empty());

    }

    @GET
    @Path("/match/matchday-number/with-stadium/list")
    public Response getMatchesByMatchdayNumberWithStadium(@DefaultValue("0") @QueryParam("page") Integer page,
                                         @DefaultValue("10") @QueryParam("size") Integer size,
                                         @DefaultValue("date_scheduled ASC") @QueryParam("order") String order,
                                         @QueryParam("number") Integer matchdayNumber,
                                         @Context HttpServletRequest request) {

        ListResult<Match> result = matchService.getMatchesByMatchdayNumberWithStadium(matchdayNumber, page, size, order);
        return EndpointUtil.getListResponse(result, page, size, Optional.empty());
    }

}
