package com.portland.providence.service;

import com.portland.common.dao.MatchdayDao;
import com.portland.common.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Component;

import com.portland.common.domain.Matchday;
import com.portland.common.domain.Season;
import com.portland.common.pojo.Result;
import com.portland.providence.dao.EntityDao;
import com.portland.providence.solr.dao.MatchdaySolrRepository;

/**
 * @author uriel Matchday Service
 */
@Component
public class MatchdayService extends AbstractService<Matchday> {

    @Autowired
    private MatchdayDao matchdayDao;

    @Autowired
    private MatchdaySolrRepository matchdaySolrRepository;


    @Override
    public EntityDao<Matchday> getDao() {
        return (EntityDao<Matchday>)matchdayDao;
    }

    @Override
    public SolrCrudRepository<Matchday, String> getSolrDao() {
        return matchdaySolrRepository;
    }

    @Override
    public Result<Matchday> getByUUID(String uuid) {
        Result<Matchday> matchday = getDao().getByUuid(uuid);
        if (!matchday.isValid()) {
            return matchday;
        }
        
        serviceUtil.setAttributeFromId(matchday.getValue().get(), Season.class);
        
        catalogFieldHelper.setCatalogValues(Matchday.class, matchday.getValue().get());
        return Result.successResult(matchday.getValue().get());
    }

    @Override
    public Result<Matchday> getByUUIDFromCache(String uuid) {
        Result<Matchday> matchday = getDao().getByUuidFromCache(uuid);
        if (!matchday.isValid()) {
            return matchday;
        }

        serviceUtil.setAttributeFromCacheById(matchday.getValue().get(), Season.class);

        catalogFieldHelper.setCatalogValues(Matchday.class, matchday.getValue().get());
        return Result.successResult(matchday.getValue().get());
    }

    @Override
    public Result<Matchday> beforeInsert(Matchday entity) {
        return Result.successResult(entity);
    }

    @Override
    public Result<Matchday> beforeUpdate(Matchday entity) {
        return Result.successResult(entity);
    }

    @Override
    public Result<Matchday> afterInsert(Matchday entity) {
        Result<Matchday> matchday = getByUUID(entity.getUuid());
        if (matchday.isValid()) {
            getSolrDao().save(matchday.getValue().get());
        }
        return Result.successResult(entity);
    }

    @Override
    public Result<Matchday> afterUpdate(Matchday entity) {
        Result<Matchday> matchday = getByUUID(entity.getUuid());
        if (matchday.isValid()) {
            getSolrDao().save(matchday.getValue().get());
        }
        return Result.successResult(entity);
    }

    @Override
    public Result<Matchday> insertAndUpdate(Matchday entity) {
        if (entity.getSeasonUuid() != null) {
            Result<Matchday> result = serviceUtil.setAttributeFromCacheByUuid(entity, Season.class);
            if (!result.isSuccessful()) {
                return result;
            }
        }
        
        return Result.successResult(entity);
    }

    @Override
    public Class<Matchday> getType() {
        return Matchday.class;
    }

    public Result<Matchday> getMatchdayByNumber(Integer number) {
        Result<Matchday> matchday = matchdayDao.getByNumber(number);
        if (!matchday.isValid()) {
            return matchday;
        }
        return Result.successResult(matchday.getValue().get());
    }

}
