package com.portland.providence.service;

import com.portland.common.dao.MatchDao;
import com.portland.common.dao.SeasonDao;
import com.portland.common.dao.VideoDao;
import com.portland.common.domain.Match;
import com.portland.common.domain.Matchday;
import com.portland.common.domain.Season;
import com.portland.common.domain.Stadium;
import com.portland.common.domain.Team;
import com.portland.common.pojo.ListResult;
import com.portland.common.pojo.Result;
import com.portland.providence.dao.EntityDao;
import com.portland.providence.solr.dao.MatchSolrRepository;
import com.portland.providence.util.PermalinkHandler;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author uriel Match Service
 */
@Component
public class MatchService extends AbstractService<Match> {

    private static final Logger logger = LoggerFactory.getLogger(MatchService.class);

    private static final Integer NUMBER_EIGHT = 8;

    private static final Integer NUMBER_SIXTEEN = 16;

    @Autowired
    private TeamService teamService;
    
    @Autowired
    private MatchDao matchDao;
    
    @Autowired
    private SeasonDao seasonDao;

    @Autowired
    private VideoDao videoDao;

    @Autowired
    private MatchdayService matchdayService;

    @Autowired
    private MatchSolrRepository matchSolrRepository;

    @Autowired
    private PermalinkHandler permalinkHandler;

    @Override
    public EntityDao<Match> getDao() {
        return (EntityDao<Match>)matchDao;
    }


    @Override
    public SolrCrudRepository<Match, String> getSolrDao() {
        return matchSolrRepository;
    }

    @Override
    public Result<Match> getByUUID(String uuid) {
        Result<Match> match = getDao().getByUuid(uuid);
        if (!match.isValid()) {
            return match;
        }
        
        setUpMatch(match.getValue().get());

        catalogFieldHelper.setCatalogValues(Match.class, match.getValue().get());
        return Result.successResult(match.getValue().get());
    }

    @Override
    public Result<Match> getByUUIDFromCache(String uuid) {
        Result<Match> match = getDao().getByUuidFromCache(uuid);
        if (!match.isValid()) {
            return match;
        }

        setUpMatchFromCache(match.getValue().get());

        catalogFieldHelper.setCatalogValues(Match.class, match.getValue().get());
        return Result.successResult(match.getValue().get());
    }

    public void setUpMatch(Match match) {
        if (match.getHomeTeamId() != null) {
            serviceUtil.setAttributeFromId(match, "homeTeam", Team.class);
            if (match.getHomeTeam() != null) {
                teamService.setUpMedia(match.getHomeTeam()); 
            }
        }
        
        if (match.getAwayTeamId() != null) {
            serviceUtil.setAttributeFromId(match, "awayTeam", Team.class);
            if (match.getAwayTeam() != null) {
                teamService.setUpMedia(match.getAwayTeam()); 
            }
        }
        
        if (match.getMatchdayId() != null) {
            serviceUtil.setAttributeFromId(match, Matchday.class);
        }
        
        if (match.getStadiumId() != null) {
            serviceUtil.setAttributeFromId(match, Stadium.class);
        }
        
        Result<Season> season = seasonDao.getSeasonForMatch(match.getUuid());
        if (season.isValid()) {
            match.setSeason(season.getValue().get());
        }

    }

    public void setUpMatchFromCache(Match match) {
        if (match.getHomeTeamId() != null) {
            serviceUtil.setAttributeFromCacheById(match, "homeTeam", Team.class);
            if (match.getHomeTeam() != null) {
                teamService.setUpMediaFromCache(match.getHomeTeam());
            }
        }

        if (match.getAwayTeamId() != null) {
            serviceUtil.setAttributeFromCacheById(match, "awayTeam", Team.class);
            if (match.getAwayTeam() != null) {
                teamService.setUpMediaFromCache(match.getAwayTeam());
            }
        }

        if (match.getMatchdayId() != null) {
            serviceUtil.setAttributeFromCacheById(match, Matchday.class);
        }

        if (match.getStadiumId() != null) {
            serviceUtil.setAttributeFromCacheById(match, Stadium.class);
        }

        Result<Season> season = seasonDao.getSeasonForMatch(match.getUuid());
        if (season.isValid()) {
            match.setSeason(season.getValue().get());
        }

    }

    public void setUpTeams(Match match) {
        if (match.getHomeTeamId() != null) {
            serviceUtil.setAttributeFromCacheById(match, "homeTeam", Team.class);
            if (match.getHomeTeam() != null) {
                teamService.setUpMediaFromCache(match.getHomeTeam());
            }
        }

        if (match.getAwayTeamId() != null) {
            serviceUtil.setAttributeFromCacheById(match, "awayTeam", Team.class);
            if (match.getAwayTeam() != null) {
                teamService.setUpMediaFromCache(match.getAwayTeam());
            }
        }

        catalogFieldHelper.setCatalogValues(Match.class, match);

    }

    @Override
    public Result<Match> beforeInsert(Match entity) {
        if (entity.getStatus() == null) {
            entity.setStatus(1);
        }
        return Result.successResult(entity);
    }

    @Override
    public Result<Match> beforeUpdate(Match entity) {
        return Result.successResult(entity);
    }

    @Override
    public Result<Match> afterInsert(Match entity) {
        Result<Match> item = getByUUID(entity.getUuid());
        if (item.isValid()) {
            getSolrDao().save(item.getValue().get());
        }
        return Result.successResult(entity);
    }

    @Override
    public Result<Match> afterUpdate(Match entity) {
        Result<Match> item = getByUUID(entity.getUuid());
        if (item.isValid()) {
            getSolrDao().save(item.getValue().get());
        }
        return Result.successResult(entity);
    }

    @Override
    public Result<Match> insertAndUpdate(Match entity) {
        if (entity.getMatchdayUuid() != null) {
            Result<Match> result = serviceUtil.setAttributeFromCacheByUuid(entity, Matchday.class);
            if (!result.isSuccessful()) {
                return result;
            }
        }
        
        if (entity.getHomeTeamUuid() != null) {
            Result<Match> result = serviceUtil.setAttributeFromCacheByUuid(entity, "homeTeam", Team.class);
            if (!result.isSuccessful()) {
                return result;
            }
        }
        
        if (entity.getAwayTeamUuid() != null) {
            Result<Match> result = serviceUtil.setAttributeFromCacheByUuid(entity, "awayTeam", Team.class);
            if (!result.isSuccessful()) {
                return result;
            }
        }
        
        if (entity.getStadiumUuid() != null) {
            Result<Match> result = serviceUtil.setAttributeFromCacheByUuid(entity, Stadium.class);
            if (!result.isSuccessful()) {
                return result;
            }
        }
        
        if (entity.getPermalink() == null && entity.getHomeTeam() != null && entity.getAwayTeam() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = formatter.format(entity.getDateScheduled());
            Optional<String> permalink = permalinkHandler.createPermalink(
                    formattedDate + "/" + entity.getHomeTeam().getName() + "-vs-" + entity.getAwayTeam().getName(), Match.class);
            if (permalink.isPresent()) {
                entity.setPermalink(permalink.get());
            }
        }
        return Result.successResult(entity);
    }

    @Override
    public Class<Match> getType() {
        return Match.class;
    }

    public ListResult<Match> getMatchesByDate(String date, Integer page, Integer size, String order) {
        LocalDate localDate = null;
        try {
            localDate = LocalDate.parse(date);
            logger.debug("Today's date: " + localDate.toString());
        } catch (Exception e) {
            logger.debug("Invalid date; message: " + e.getMessage());
            return ListResult.failedResult(getType(), "Invalid date; message: " + e.getMessage());
        }

        ListResult<Match> matches = matchDao.getMatchesByDate(localDate.toString(), page, size, order);

        if (!matches.isValid()) {
            return ListResult.failedResult(getType(), "Error getting today's list of matches; message: " + matches.getMessage());
        }

        return ListResult.successResult(getMatchesWithTeams(matches.getValues().get()).getValues().get(), matches.getCount());
    }

    public ListResult<Match> getMatchesByTeamBetweenPastDateAndFutureDate(String teamUuid, Integer page, Integer size, String order) {
        LocalDateTime dateNow = LocalDateTime.now();

        LocalDateTime pastDate = dateNow.minusDays(NUMBER_EIGHT);
        LocalDateTime futureDate = dateNow.plusDays(NUMBER_SIXTEEN);

        String pastDateStr;
        String futureDateStr;

        Result<Team> team = teamService.getByUUIDFromCache(teamUuid);

        if (!team.isValid()) {
            return ListResult.failedResult(getType(), "Doesn't exist team");
        }

        try {
            pastDateStr = pastDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            futureDateStr = futureDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (Exception e) {
            logger.debug("Could not parse the dates");
            return ListResult.failedResult(getType(), "Could not parse the dates");
        }

        ListResult<Match> matches = matchDao.getMatchesByTeamBetweenPastDateAndFutureDate(team.getValue().get().getId(),
                pastDateStr, futureDateStr, page, size, order);

        if (!matches.isValid()) {
            return ListResult.failedResult(getType(), "Error getting list of matches; message: " + matches.getMessage());
        }

        return ListResult.successResult(getMatchesWithNestedObjectsFromCache(matches.getValues().get()).getValues().get(), matches.getCount());

    }

    public ListResult<Match> getMatchesWithTeams(List<Match> list) {
        List<Match> matchList = new ArrayList<>();
        for (Match match:list) {
            setUpTeams(match);
            matchList.add(match);
        }
        return ListResult.successResult(matchList, matchList.size());
    }

    public ListResult<Match> getMatchesWithNestedObjectsFromCache(List<Match> list) {
        List<Match> matchList = new ArrayList<>();
        for (Match match:list) {
            setUpMatchFromCache(match);
            matchList.add(match);
        }
        return ListResult.successResult(matchList, matchList.size());
    }

    public Result<Match> getLiveMatchByTeamUuid(String teamUuid) {

        Result<Team> team = teamService.getByUUIDFromCache(teamUuid);
        if (!team.isValid()) {
            return Result.failedResult(getType(), "Doesn't exist team");
        }

        Result<Match> match = matchDao.getLiveMatchByTeamId(team.getValue().get().getId());
        if (!match.isValid()) {
            return Result.failedResult(getType(), "Error getting live match given teamUuid: " + teamUuid + "; Message: " + match.getMessage());
        }

        DateTime dateNow = DateTime.now();
        DateTime dateMatch;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStartMatch;

        try {

            if (match.getValue().get().getDateStart() != null) {
                dateStartMatch = formatter.format(match.getValue().get().getDateStart());
            } else {
                if (match.getValue().get().getDateScheduled() != null) {
                    dateStartMatch = formatter.format(match.getValue().get().getDateScheduled());
                } else {
                    logger.debug("There isn't dateStart or dateScheduled");
                    return Result.failedResult(getType(), "There isn't dateStart or dateScheduled");
                }
            }

            dateMatch = new DateTime(formatter.parse(dateStartMatch));

        } catch (ParseException e) {
            logger.debug("Error parsing dates");
            return Result.failedResult(getType(), "Error parsing dates");
        }

        /*
        if (!dateNow.toLocalDate().isEqual(dateMatch.toLocalDate())) {
            logger.debug("Error calculating live minute; Doesn't match dates: Current Date: " + dateNow.toLocalDate().toString()
                    + " Match date: " + dateMatch.toLocalDate().toString());
            return Result.failedResult(getType(), "Error calculating live minute; Doesn't match dates: Current Date: "
                    + dateNow.toLocalDate().toString()
            + " Match date: " + dateMatch.toLocalDate().toString());
        }*/

        Integer liveMinutes = Minutes.minutesBetween(dateMatch, dateNow).getMinutes();
        if (liveMinutes < 0) {
            liveMinutes = 0;
        }
        match.getValue().get().setLiveMinute(liveMinutes);

        setUpMatchFromCache(match.getValue().get());

        return match;

    }

    public ListResult<Match> getMatchesBetweenPastDateAndFutureDate(Integer page, Integer size, String order) {
        LocalDateTime dateNow = LocalDateTime.now();

        LocalDateTime pastDate = dateNow.minusDays(NUMBER_EIGHT);
        LocalDateTime futureDate = dateNow.plusDays(NUMBER_SIXTEEN);

        String pastDateStr;
        String futureDateStr;

        try {
            pastDateStr = pastDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            futureDateStr = futureDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (Exception e) {
            logger.debug("Could not parse the dates");
            return ListResult.failedResult(getType(), "Could not parse the dates");
        }

        ListResult<Match> matches = matchDao.getMatchesBetweenPastDateAndFutureDate(
                pastDateStr, futureDateStr, page, size, order);

        if (!matches.isValid()) {
            return ListResult.failedResult(getType(), "Error getting list of matches; message: " + matches.getMessage());
        }

        return ListResult.successResult(getMatchesWithNestedObjectsFromCache(matches.getValues().get()).getValues().get(), matches.getCount());

    }

    public Result<Match> updateMatchScore(Long matchId) {

        Result<Match> match = getDao().getById(matchId);

        if (!match.isValid()) {
            logger.debug("Could not get match by id");
            return Result.failedResult(getType(), "Could not get match by id");
        }

        if (match.getValue().get().getAwayTeamId() == null || match.getValue().get().getHomeTeamId() == null) {
            logger.debug("Could not get match's teams");
            return Result.failedResult(getType(), "Could not get match's teams");
        }

        Result<Integer> homeGoals = videoDao.getNumberOfGoalsByTeamIdAndMatchId(match.getValue().get().getHomeTeamId(),
                match.getValue().get().getId());
        if (!homeGoals.isValid()) {
            logger.debug("Error getting goals of home team");
            return Result.failedResult(getType(), "Error getting goals of home team");
        }

        Result<Integer> awayGoals = videoDao.getNumberOfGoalsByTeamIdAndMatchId(match.getValue().get().getAwayTeamId(),
                match.getValue().get().getId());
        if (!awayGoals.isValid()) {
            logger.debug("Error getting goals of away team");
            return Result.failedResult(getType(), "Error getting goals of away team");
        }

        match.getValue().get().setHomeScore(homeGoals.getValue().get());
        match.getValue().get().setAwayScore(awayGoals.getValue().get());

        Result<Match> matchUpdated = matchDao.update(match.getValue().get());
        if (!matchUpdated.isValid()) {
            logger.debug("Error updating match; message: " + matchUpdated.getMessage());
            return Result.failedResult(getType(), "Error updating match; message: " + matchUpdated.getMessage());
        }

        return matchUpdated;
    }

    public ListResult<Match> getMatchesByMatchdayNumberWithStadium(Integer matchdayNumber, Integer page, Integer size, String order) {

        if (matchdayNumber == null) {
            logger.debug("Error getting matchday number");
            return ListResult.failedResult(getType(), "Error getting matchday number");
        }

        Result<Matchday> matchday = matchdayService.getMatchdayByNumber(matchdayNumber);

        if (!matchday.isValid()) {
            return ListResult.failedResult(getType(), "Doesn't exist matchday with number " + matchdayNumber);
        }

        ListResult<Match> matches = matchDao.getMatchesByMatchdayWithStadium(matchday.getValue().get().getId(), page, size, order);

        if (!matches.isValid()) {
            return ListResult.failedResult(getType(), "Error getting list of matches; message: " + matches.getMessage());
        }

        return ListResult.successResult(getMatchesWithNestedObjectsFromCache(matches.getValues().get()).getValues().get(), matches.getCount());
    }

}
